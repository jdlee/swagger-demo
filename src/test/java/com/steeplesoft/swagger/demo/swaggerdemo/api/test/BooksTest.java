package com.steeplesoft.swagger.demo.swaggerdemo.api.test;

import com.steeplesoft.swagger.demo.swaggerdemo.api.BooksApi;
import com.steeplesoft.swagger.demo.swaggerdemo.model.Book;
import com.steeplesoft.swagger.demo.swaggerdemo.model.Books;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.eclipse.persistence.oxm.MediaType;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.FileAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author jdlee
 */
public class BooksTest extends Arquillian {

    @ArquillianResource
    private URL deploymentURL;

    @Deployment(testable = false)
    public static Archive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addAsLibraries(Maven.resolver()
                        .loadPomFromFile("pom.xml").resolve("io.swagger:swagger-core")
                        .withTransitivity().as(File.class))
                .addAsLibraries(Maven.resolver()
                        .loadPomFromFile("pom.xml").resolve("io.swagger:swagger-jaxrs")
                        .withTransitivity().as(File.class))
                .addAsWebInfResource(new FileAsset(new File("src/main/webapp/WEB-INF/web.xml")), "web.xml")
                .addPackage(Books.class.getPackage())
                .addPackage(BooksApi.class.getPackage());
//                .addClass(HelloWorld.class);
    }

    @Test
    public void sayHelloTest() {
//        Assert.assertEquals(helloWorld.sayHello(), "Hello World");
    }

    Client client = ClientBuilder.newClient();

    @Test
    public void foo() throws URISyntaxException {
        Assert.assertNotNull(deploymentURL);
        WebTarget webTarget = client.target(deploymentURL.toURI());
        
        Book book = new Book();
        book.setTitle("test");
        Entity<Book> entity = Entity.json(book);

        Response response = webTarget.path("resources").path("books")
                .request()
                .post(entity);
        System.out.println("\n\n\n\n\n\n\n\n\n"
                + response.readEntity(String.class)
                + "\n\n\n\n\n\n\n\n\n");
    }
}
