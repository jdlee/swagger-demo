package com.steeplesoft.swagger.demo.swaggerdemo.api.factories;

import com.steeplesoft.swagger.demo.swaggerdemo.api.AuthorsApiService;
import com.steeplesoft.swagger.demo.swaggerdemo.api.impl.AuthorsApiServiceImpl;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-02-03T15:58:35.352-06:00")
public class AuthorsApiServiceFactory {
    private final static AuthorsApiService service = new AuthorsApiServiceImpl();

    public static AuthorsApiService getAuthorsApi() {
        return service;
    }
}
