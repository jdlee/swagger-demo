package com.steeplesoft.swagger.demo.swaggerdemo.api.impl;

import com.steeplesoft.swagger.demo.swaggerdemo.api.*;
import com.steeplesoft.swagger.demo.swaggerdemo.model.*;

import com.steeplesoft.swagger.demo.swaggerdemo.model.Book;
import com.steeplesoft.swagger.demo.swaggerdemo.model.Books;
import com.steeplesoft.swagger.demo.swaggerdemo.model.Error;

import java.util.List;
import com.steeplesoft.swagger.demo.swaggerdemo.api.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.validation.constraints.*;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-02-03T15:58:35.352-06:00")
public class BooksApiServiceImpl extends BooksApiService {
    @Override
    public Response addBook(Book book, SecurityContext securityContext) throws NotFoundException {
        // do some magic!
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
    }
    @Override
    public Response getBook(Integer book, SecurityContext securityContext) throws NotFoundException {
        // do some magic!
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
    }
    @Override
    public Response getBooks(SecurityContext securityContext) throws NotFoundException {
        // do some magic!
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
    }
    @Override
    public Response updateBook(Book book, SecurityContext securityContext) throws NotFoundException {
        // do some magic!
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
    }
}
