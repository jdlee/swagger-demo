package com.steeplesoft.swagger.demo.swaggerdemo.api.factories;

import com.steeplesoft.swagger.demo.swaggerdemo.api.BooksApiService;
import com.steeplesoft.swagger.demo.swaggerdemo.api.impl.BooksApiServiceImpl;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-02-03T15:58:35.352-06:00")
public class BooksApiServiceFactory {
    private final static BooksApiService service = new BooksApiServiceImpl();

    public static BooksApiService getBooksApi() {
        return service;
    }
}
