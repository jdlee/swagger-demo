package com.steeplesoft.swagger.demo.swaggerdemo.api.impl;

import com.steeplesoft.swagger.demo.swaggerdemo.api.*;
import com.steeplesoft.swagger.demo.swaggerdemo.model.*;

import com.steeplesoft.swagger.demo.swaggerdemo.model.Authors;

import java.util.List;
import com.steeplesoft.swagger.demo.swaggerdemo.api.NotFoundException;

import java.io.InputStream;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.validation.constraints.*;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJerseyServerCodegen", date = "2017-02-03T15:58:35.352-06:00")
public class AuthorsApiServiceImpl extends AuthorsApiService {
    @Override
    public Response getAuthors(SecurityContext securityContext) throws NotFoundException {
        // do some magic!
        return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
    }
}
